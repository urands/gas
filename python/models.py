import datetime

from sqlalchemy import Column, Integer, DateTime, BigInteger, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database

Base = declarative_base()


class GasPrice(Base):
    __tablename__ = 'gasprice'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)  #
    value = Column(BigInteger)

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return '<GasPrice(%s %s, %s)>' % (self.id, self.date, self.value)


class DataAccessLayer:
    session = None
    engine = None
    conn_string = None
    running = False

    def connect(self, conn_string):
        self.engine = create_engine(
            conn_string or self.conn_string, echo=False)
        if not database_exists(self.engine.url):
            create_database(self.engine.url)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()
        self.running = True

    def close(self):
        self.running = False
        if self.session:
            self.session.close()

    def insert(self, gasvalue):
        gp = GasPrice(gasvalue)
        if self.session:
            self.session.add(gp)
            self.session.commit()
        return gp.id


dal = DataAccessLayer()
