import datetime
import os
import unittest
from threading import Thread
from time import sleep

from app import main
from models import dal, GasPrice
from sqlalchemy_utils import drop_database

from gas import gas


class Test1_ORM(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        dal.connect('sqlite:///:memory:')

    def tearDown(self):
        dal.session.rollback()
        dal.session.close()

    def test_gasprice_blank(self):
        results = dal.session.query(GasPrice).all()
        self.assertEqual(results, [])

    def test_gasprice_min_max_time_insert(self):
        start = int(datetime.datetime.now(datetime.timezone.utc).timestamp())
        val = 1000
        dal.insert(val)
        minGas = GasPrice(21000)
        maxGas = GasPrice(10 ** 18)
        dal.session.bulk_save_objects([minGas, maxGas])
        dal.session.commit()
        stop = datetime.datetime.now(datetime.timezone.utc).timestamp()
        results = dal.session.query(GasPrice).all()

        self.assertEqual(len(results), 3)
        self.assertEqual(val, results[0].value)
        self.assertLessEqual(start, results[0].date.replace(
            tzinfo=datetime.timezone.utc).timestamp())
        self.assertGreaterEqual(stop, results[0].date.replace(
            tzinfo=datetime.timezone.utc).timestamp())
        self.assertEqual(minGas.value, results[1].value)
        self.assertLessEqual(start, results[1].date.replace(
            tzinfo=datetime.timezone.utc).timestamp())
        self.assertGreaterEqual(stop, results[1].date.replace(
            tzinfo=datetime.timezone.utc).timestamp())
        self.assertEqual(maxGas.value, results[2].value)
        self.assertLessEqual(start, results[2].date.replace(
            tzinfo=datetime.timezone.utc).timestamp())
        self.assertGreaterEqual(stop, results[2].date.replace(
            tzinfo=datetime.timezone.utc).timestamp())


class Test2_GasPrice(unittest.TestCase):

    def test_provider(self):
        g = gas(os.environ.get('WEB3_PROVIDER', 'http://127.0.0.1:8545'))
        self.assertTrue(g.is_connected())

    def test_get(self):
        g = gas(os.environ.get('WEB3_PROVIDER', 'http://127.0.0.1:8545'))

        gasvalue = os.environ.get('GASSVALUE', 2202455)

        key = os.environ.get(
            'ACCOUNT_1_PK',
            '0x70f1384b24df3d2cdaca7974552ec28f055812ca5e4da7a0ccd0ac0f8a4a9b00')

        transaction = {
            'from': g.w3.eth.accounts[0],
            'to': g.w3.eth.accounts[1],
            'value': 100000,
            'gas': 2000000,
            'gasPrice': gasvalue,
            'nonce': g.w3.eth.getTransactionCount(g.w3.eth.accounts[0]),
            'chainId': 1
        }

        signed_transaction = g.w3.eth.account.signTransaction(transaction, key)
        g.w3.eth.sendRawTransaction(
            signed_transaction.rawTransaction)
        self.assertEqual(g.get(), gasvalue)

    def test_strategy(self):
        g = gas(os.environ.get('WEB3_PROVIDER', 'http://127.0.0.1:8545'))
        strategy_value = os.environ.get('STRATEGY_VALUE', 20000000000)
        self.assertEqual(g.strategy(), strategy_value)


class Test3_MainFunctional(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        os.environ["CONN_STRING"] = 'sqlite:///test.db'
        dal.connect(os.environ.get('CONN_STRING', 'sqlite:///:memory:'))
        dal.session.close()

    def test_main(self):
        wait_time = int(os.environ.get('TIMEINTERVAL', 60))
        print(wait_time)
        p = Thread(target=main)
        p.start()
        sleep(wait_time * 3.5)
        dal.running = False
        sleep(wait_time * 1.5)
        dal.connect(os.environ.get('CONN_STRING', 'sqlite:///:memory:'))
        results = dal.session.query(GasPrice).all()
        print(len(results))
        self.assertEqual(len(results), 3)
        dal.close()
        drop_database(dal.engine.url)


if __name__ == '__main__':
    wait_time = int(os.environ.get('TIMEINTERVAL', 60))
    sleep(wait_time)
    unittest.main()
