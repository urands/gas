from web3 import Web3
from web3.auto.infura import w3
from web3.gas_strategies.time_based import medium_gas_price_strategy
from web3.gas_strategies.rpc import rpc_gas_price_strategy


class gas:
    def __init__(self, provider):
        if (provider != 'infura'):
            self.w3 = Web3(Web3.HTTPProvider(provider))
        else:
            self.w3 = w3

        self.w3.eth.setGasPriceStrategy(medium_gas_price_strategy)

    def is_connected(self):
        return self.w3.isConnected()

    def get(self):
        return self.w3.eth.generateGasPrice()

    def strategy(self):
        return rpc_gas_price_strategy(self.w3)
