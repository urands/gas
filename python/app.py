#!/usr/local/bin/python
import os
import time

from models import dal

from gas import gas


def main():
    wait_time = int(os.environ.get('TIMEINTERVAL', 60))
    provider = os.environ.get('WEB3_PROVIDER', 'infura')
    conn_string = os.environ.get('CONN_STRING', 'sqlite:///:memory:')
    verbose = os.environ.get('VERBOSE', 1)

    print('Connection string:', conn_string)
    print('Provider:', provider)
    print('Timeinterval:', wait_time)
    print('Verbose:', verbose)

    dal.connect(conn_string)

    g = gas(provider)

    print('Connection:', g.is_connected())

    time.sleep(wait_time)

    while (g.is_connected() and dal.running):
        start = time.time()
        gp = g.get()
        dal.insert(gp)
        if (verbose):
            print(time.ctime(), 'GAS Price:', gp, 'wei')
        wait = time.time() - start
        if (wait < wait_time):
            time.sleep(wait_time - wait)

    dal.close()


if __name__ == '__main__':
    print('Start application')
    main()
